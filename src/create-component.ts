export interface Component{
    renderHtml: () => string;
}

const noop = () => {};
export function createComponent(options: { name: string, scss: string }) {
    const styleNode = document.createElement('style');
    const template = document.createElement('template');
    return function (clazz) {
        const connectedCallback = clazz.prototype.connectedCallback || noop;
        clazz.prototype.connectedCallback = function () {
            this.attachShadow({mode: 'open'})

            styleNode.textContent = options.scss;
            template.innerHTML = this.renderHtml()
            const clone = document.importNode(template.content, true);
            this.shadowRoot.append(styleNode, clone)
            connectedCallback.call(this)
        }
        window.customElements.define(options.name, clazz);
    }
}
