import template from'./button.html';
import style from './button.scss';

class Button extends HTMLElement {
    private customName = this.getAttribute('custom-name')
    private wrapper = document.createElement('div')

    private event = new CustomEvent('myEvent', {
       detail: {
           bubbles: true,
           test: 'Hello from component'
       }
    })

    static get observedAttributes() {
        return ['custom-name'];
    }
    constructor() {
        super();
        this.attachShadow({mode: 'open'});
        const styleNode = document.createElement('style');
        styleNode.textContent = style.toString();
        this.wrapper.innerHTML = template.replace('{{name}}', this.customName ?? 'EMPTY')
        this.shadowRoot.append(styleNode, this.wrapper);
    }

    connectedCallback() {
        console.log('Custom square element added to page.');

    }

    disconnectedCallback() {
        console.log('Custom square element removed from page.');
    }

    adoptedCallback() {
        console.log('Custom square element moved to new page.');
    }

    attributeChangedCallback(name, oldValue, newValue) {
        console.log('Custom square element attributes changed.');
        this.shadowRoot.removeChild(this.wrapper)
        this.wrapper.innerHTML = template.replace('{{name}}', newValue ?? 'EMPTY')
        this.shadowRoot.append(this.wrapper)
        this.dispatchEvent(this.event)
    }
}

window.customElements.define('mtx-button', Button)
