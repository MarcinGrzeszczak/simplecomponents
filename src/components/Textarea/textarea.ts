import { Component, createComponent } from '../../create-component';
import style from './textarea.scss'

@createComponent({
    name: 'mtx-textarea',
    scss: style.toString()
})
export class TextareaComponent extends HTMLElement implements Component{
    renderHtml(): string {
        return '<textarea width="200" height="200"></textarea>';
    }
}
