const path = require('path')
const CopyPlugin = require('copy-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
module.exports = {
    mode: "production",
    entry: "./src/app.ts",
    output: {
        filename: "bundle.js",
        path: path.resolve(__dirname, 'dist'),
        clean: true
    },
    resolve: {
        extensions: [".ts", ".tsx", ".js",  ".css", ".scss"]
    },
    module: {
        rules: [
            {
                test: /\.html$/i,
                exclude: /node_modules/,
                loader: "html-loader"
            },
            {
                test: /\.s?[ac]ss$/i,
                exclude: /node_modules/,
                use: [
                    //'style-loader', // creates style nodes from JS strings
                    'css-loader',
                    // {
                    //     loader: 'css-loader', // translates CSS into CommonJS
                    //     options: {
                    //         importLoaders: 1,
                    //         modules: true
                    //     }
                    // },
                    'postcss-loader', // post process the compiled CSS
                    'sass-loader' // compiles Sass to CSS, using Node Sass by default
                ],
            },
            {
                test: /\.ts$/,
                exclude: /node_modules/,
                loader: "ts-loader"
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(),
        new CopyPlugin({
            patterns: [
                {from: path.resolve(__dirname, 'public',), to: path.resolve(__dirname, 'dist')}
            ]
        })
    ]
}
